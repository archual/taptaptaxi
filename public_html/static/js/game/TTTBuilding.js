var TTTBuilding = (function() {
    
    function TTTBuilding(phaserGame, x, y) {
        // We need to call the initializer of the 'super' class.
        Phaser.Sprite.call(this, phaserGame, x, y, 'gameAssets', this.getRandomBase());
        
        this.game = phaserGame;
        
        // Buildings variables.
        this.color = ['brown', 'beige', 'red', 'grey'];
        this.windowType = ['big', 'small'];
        
        // Add floors;
        this.buildFloors(Math.round(Math.random() * 4) + 1);
    };
    
    // (inheritance).
    TTTBuilding.prototype = Object.create(Phaser.Sprite.prototype);
    TTTBuilding.prototype.constructor = TTTBuilding;
    
    
    // Add floors function.
    TTTBuilding.prototype.buildFloors = function(numberOfFloors) {
        // Keep a refrence to the previous floor.
        var prevFloor = this;
        
        // Get random color & random window type.
        
        var color = this.color[this.getRandomVariations(this.color.length) - 1];
        var windowType = this.windowType[this.getRandomVariations(this.windowType.length) - 1];
        
        for (var i = 0; i < numberOfFloors; i++) {
            var floor = this.game.make.sprite(
                    0,
                    0, 
                    'gameAssets',
                    this.getRandomFloorColor(color, windowType)
                );
            floor.anchor.setTo(0.5, 1);
            
            // There's a height diffrence on the base tiles and the floor tiles, 
            // hence this check.
            if (prevFloor == this) {
                floor.y = prevFloor.y - prevFloor.height / 2 - 12;
            } else {
                floor.y = prevFloor.y - prevFloor.height / 2 + 10;
            }
            
            this.addChild(floor);
            prevFloor = floor;
        }
    };
    
    // Get random base.
    TTTBuilding.prototype.getRandomBase = function() {
        var numberOfVariations = 4;
        var num = this.getRandomVariations(numberOfVariations);
        return 'building_base_' + num;
    }
    
    // Get random floors.
    TTTBuilding.prototype.getRandomFloorColor = function(color, windowType) {
        var numberOfVariations = 2;
        var num = this.getRandomVariations(numberOfVariations);
        return 'building_middle_' + windowType + '_' + color + '_' + num;
    }
    
    // Get random variations.
    TTTBuilding.prototype.getRandomVariations = function(variations) {
        return Math.ceil(Math.random() * variations - 1) + 1;
    }
    
    return TTTBuilding;
    
})();