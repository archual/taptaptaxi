var TTTGame = (function() {
    var ANGLE = 26.55;
    var TILE_WIDTH = 68;
    var TILE_HEIGHT = 63;
    var SPEED = 5;
    var TAXI_START_X = 30;
    var JUMP_HEIGHT = 7;
    var EASING = 15;
    
    // Game constructor
    function TTTGame(phaserGame) {
        this.game = phaserGame;
        
        this.arrTiles = [];
        this.arrLayers = [];
        this.numberOfIterations = 0;
        this.roadStartPosition = {
            x: GAME_WIDTH + 100,
            y: GAME_HEIGHT / 2 - 100
        }
        
        this.logo = undefined;
        this.tapToStart = undefined;
        
        this.mouseTouchDown = false;
        this.taxi = undefined;
        this.taxiX = TAXI_START_X;
        this.taxiTargetX = 0;
        this.jumpSpeed = JUMP_HEIGHT;
        this.isJumping = false;
        this.currentJumpHeight = 0;
        
        // Road variables
        this.roadCount = 0;
        this.nextObstacleIndex = 0;
        this.arrObstacles = [];
        
        // Green queue.
        this.nextQueueIndex = 0;
        this.rightQueue = [];
        
        // Game states.
        this.hasStarted = false;
        this.isDead = false;
        this.gameOverGraphic = undefined;
        
        this.blackOverlay = undefined;
        
        this.buttonRestart = undefined;
        
        // Counter.
        this.scoreCount = 0;
        
        // Audio.
        this.sfx = {};
    }
    
    // Init function
    TTTGame.prototype.init = function() {
         this.game.stage.backgroundColor = '#9bd3e1';
        
        // Debug plugin
        this.game.add.plugin(Phaser.Plugin.Debug);
    };
    
    // Reset game.
    TTTGame.prototype.reset = function() {
        // Game variables.
        this.hasStarted = false;
        this.isDead = false;
        
        // Logo.
        this.logo.visible = true;
        this.counter.visible = false;
        
        // Tap to start.
        this.tapToStart.visible = true;
        this.tapToStart.blinker.startBlinking();
        
        // Jump variables.
        this.jumpSpeed = JUMP_HEIGHT;
        this.isJumping = false;
        this.currentJumpHeight = 0;
        
        // Road variables.
        this.nextObstacleIndex = 0;
        this.arrObstacles = [];
        
        this.mouseTouchDown = false;
        
        // Taxi properties.
        this.game.tweens.removeFrom(this.taxi);
        this.taxiX = TAXI_START_X;
        this.taxi.rotation = 0;
        this.taxiTargetX = 0;
        
        // Counter.
        this.scoreCount = 0;
        this.counter.setScore(0, false);
        
        // Reset graphic visibility.
        this.blackOverlay.visible = false;
        this.gameOverGraphic.visible = false;
        
        this.buttonRestart.visible = false;
    };
    
    // Start game.
    TTTGame.prototype.startGame = function() {
        this.hasStarted = true;
        this.logo.visible = false;
        this.counter.visible = true;
        this.tapToStart.visible = false;
        this.tapToStart.blinker.stopBlinking();
    };
    
    // Preloader
    TTTGame.prototype.preload = function() {
        // This.game.load - instance of Phaser loader class.
        // this.game.load.image('logo', 'static/img/assets/logo.png');
        // this.game.load.image('tapToStart', 'static/img/assets/tapToStart.png');
        
        // this.game.load.image('empty', 'static/img/assets/empty.png');
        // this.game.load.image('tile_road_1', 'static/img/assets/tile_road_1.png');
        // this.game.load.image('taxi', 'static/img/assets/taxi.png');
        // this.game.load.image('obstacle_1', 'static/img/assets/obstacle_1.png');
        // this.game.load.image('gameover', 'static/img/assets/gameOver.png');
        
        // this.game.load.image('green_end', 'static/img/assets/green_end.png');
        // this.game.load.image('green_middle_empty', 'static/img/assets/green_middle_empty.png');
        // this.game.load.image('green_middle_tree', 'static/img/assets/green_middle_tree.png');
        // this.game.load.image('green_start', 'static/img/assets/green_start.png');
        
        // this.game.load.image('water', 'static/img/assets/water.png');
        
        // // buildings.
        // this.game.load.image('building_base_1', 'static/img/assets/buildingTiles_124.png'); // Grey
        // this.game.load.image('building_base_2', 'static/img/assets/buildingTiles_107.png'); // Semi-Red
        // this.game.load.image('building_base_3', 'static/img/assets/buildingTiles_100.png'); // Green
        // this.game.load.image('building_base_4', 'static/img/assets/buildingTiles_099.png'); // Full red
        
        // this.game.load.image('building_middle_small_brown_1', 'static/img/assets/buildingTiles_047.png'); // Small windows brown
        // this.game.load.image('building_middle_small_brown_2', 'static/img/assets/buildingTiles_038.png'); // Big windows brown
        // this.game.load.image('building_middle_big_brown_1', 'static/img/assets/buildingTiles_000.png'); // 2 Big windows brown
        // this.game.load.image('building_middle_big_brown_2', 'static/img/assets/buildingTiles_007.png'); // 1 Big window brown
        
        // this.game.load.image('building_middle_small_beige_1', 'static/img/assets/buildingTiles_051.png'); // Small windows beige
        // this.game.load.image('building_middle_small_beige_2', 'static/img/assets/buildingTiles_044.png'); // Big windows beige
        // this.game.load.image('building_middle_big_beige_1', 'static/img/assets/buildingTiles_008.png'); // 2 Big windows beige
        // this.game.load.image('building_middle_big_beige_2', 'static/img/assets/buildingTiles_015.png'); // 1 Big window beige
        
        // this.game.load.image('building_middle_small_red_1', 'static/img/assets/buildingTiles_054.png'); // Small windows red
        // this.game.load.image('building_middle_small_red_2', 'static/img/assets/buildingTiles_049.png'); // Big windows red
        // this.game.load.image('building_middle_big_red_1', 'static/img/assets/buildingTiles_016.png'); // 2 Big windows red
        // this.game.load.image('building_middle_big_red_2', 'static/img/assets/buildingTiles_023.png'); // 1 Big window red
        
        // this.game.load.image('building_middle_small_grey_1', 'static/img/assets/buildingTiles_056.png'); // Small windows grey
        // this.game.load.image('building_middle_small_grey_2', 'static/img/assets/buildingTiles_053.png'); // Big windows grey
        // this.game.load.image('building_middle_big_grey_1', 'static/img/assets/buildingTiles_024.png'); // 2 Big windows grey
        // this.game.load.image('building_middle_big_grey_2', 'static/img/assets/buildingTiles_031.png'); // 1 Big window grey
        
        // Texture atlas.
        // Game.
        this.game.load.atlasJSONArray(
            'gameAssets',
            'static/img/spritesheets/gameAssets.png',
            'static/img/spritesheets/gameAssets.json'
        );
        
        
        // Numbers for counter.
        this.game.load.atlasJSONArray(
            'numbers',
            'static/img/spritesheets/numbers.png',
            'static/img/spritesheets/numbers.json'
        );
        
        // Play buttons.
        this.game.load.atlasJSONArray(
            'playButton',
            'static/img/spritesheets/playButton.png',
            'static/img/spritesheets/playButton.json'
        );
        
        // Audio;
        this.game.load.audio('hit', 'static/audio/hit.wav');
        this.game.load.audio('jump', 'static/audio/jump.wav');
        this.game.load.audio('score', 'static/audio/score.wav');
    };
    
    // Update
    TTTGame.prototype.update = function() {
        if (this.game.input.activePointer.isDown) {
            if (!this.mouseTouchDown) {
                this.touchDown();
            }
        } else {
            if (this.mouseTouchDown) {
                this.touchUp();
            }
        }

        if (this.roadCount > this.nextQueueIndex) {
            this.generateRightQueue();
        }
        
        if (!this.isDead) {
            if (this.isJumping) {
                this.taxiJump();
            }
            
            // Calculate taxiX wirh difficulty.
            this.calculateTaxiPosition();
            
            var pointOnRoad = this.calculatePositionOnRoadWithXposition(this.taxiX);
            this.taxi.x = pointOnRoad.x;
            this.taxi.y = pointOnRoad.y + this.currentJumpHeight;
    
            this.checkObstacles();
        }

        
        this.numberOfIterations++;
        if (this.numberOfIterations > TILE_WIDTH / SPEED) {
            this.numberOfIterations = 0;
            this.generateRoad();
        }
        
        // if (this.hasStarted) {
            this.moveTilesWithSpeed(SPEED);
        // }
    };
    
    // Create
    TTTGame.prototype.create = function() {
        // Layers.
        var numberOfLayers = 9;
        
        for (var i = 0; i < numberOfLayers; i++) {
            var layer = new Phaser.Sprite(this.game, 0, 0);
            this.game.world.addChild(layer);
            
            // this.arrTiles will now hold layers.
            this.arrLayers.push(layer);
        }
        
        this.generateRoad();

        // Logo.
        this.logo = this.game.add.sprite(0, 0, 'gameAssets', 'logo');
        this.logo.anchor.setTo(0.5, 0.5);
        this.logo.x = this.game.world.centerX;
        this.logo.y = 100;
        
        // Tap to start.
        this.tapToStart = this.game.add.sprite(0, 0, 'gameAssets', 'tapToStart');
        this.tapToStart.anchor.setTo(0.5, 0.5);
        this.tapToStart.x = GAME_WIDTH / 2;
        this.tapToStart.y = GAME_HEIGHT - 60;
        
        this.tapToStart.blinker = new TTTBlinker(this.game, this.tapToStart);

        this.taxi = new Phaser.Sprite(this.game, GAME_WIDTH / 2, GAME_HEIGHT / 2, 'gameAssets', 'taxi');
        this.game.world.addChild(this.taxi);
        this.taxi.anchor.setTo(0.5, 1);
        
        this.blackOverlay = this.game.add.graphics(0, 0);
        this.blackOverlay.beginFill(0x000000, 1);
        this.blackOverlay.drawRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        this.blackOverlay.endFill();
        this.blackOverlay.visible = false;
        
        var x = this.game.world.centerX;
        var y = this.game.world.centerY - 100;
        this.gameOverGraphic = new Phaser.Sprite(this.game, x, y, 'gameAssets', 'gameOver');
        this.game.add.existing(this.gameOverGraphic);
        this.gameOverGraphic.anchor.setTo(0.5, 0.5);
        this.gameOverGraphic.visible = false;
        
        this.buttonRestart = new Phaser.Button(
            this.game,
            0, 
            0,
            'playButton',// key
            this.restart,// Callback.
            this, //context
            'default', // Over
            'default', //out
            'hover', // down
            'default' // Up
        );
        
        this.game.add.existing(this.buttonRestart);
        this.buttonRestart.anchor.setTo(0.5, 0.5);
        this.buttonRestart.x = GAME_WIDTH / 2;
        this.buttonRestart.y = this.gameOverGraphic.y + this.gameOverGraphic.height / 2 + 50;
        
        this.counter = new TTTCounter(this.game, 0, 0);
        this.game.add.existing(this.counter);
        this.counter.x = GAME_WIDTH / 2;
        this.counter.y = 40;
        this.reset();
        this.generateLevel();
        
        // Audio.
        this.sfx = {
            hit: this.game.add.audio('hit'),
            jump: this.game.add.audio('jump'),
            score: this.game.add.audio('score')
        };
    };
    
    // Generate level.
    TTTGame.prototype.generateLevel = function() {
        var i = 0;
        // Calculate how many tiles fit on screen and add 2 just to be safe.
        var numberOfTiles = Math.ceil(GAME_WIDTH / TILE_WIDTH) + 2;
        
        while (i <= numberOfTiles) {
            
            this.generateRoad();
            if (i != numberOfTiles) {
                // Move the tiles by TILE_WIDTH.
                this.moveTilesWithSpeed(TILE_WIDTH);
            };
            
            i++;
        }
    };
    
    // Create tiles.
    TTTGame.prototype.createTileAtIndex = function(tile, layer) {

        var sprite = new Phaser.Sprite(this.game, 0, 0, 'gameAssets', tile);
        
        this.addTileAtIndex(sprite, layer);
        
        return sprite;
    };
    
    // Configure the sprite here and add it to the correct layer.
    TTTGame.prototype.addTileAtIndex = function(sprite, index) {
        sprite.anchor.setTo(0.5, 1);
        
         var middle_layer = 4;
        
        // <0 if it's a layer below the middle
        // > 0 it's a layer above the middle
        var offset = index - middle_layer;
        
        sprite.x = this.roadStartPosition.x;
        sprite.y = this.roadStartPosition.y + offset * TILE_HEIGHT;
        
        this.arrLayers[index].addChildAt(sprite, 0);
    };
    
    // Generate road
    TTTGame.prototype.generateRoad = function() {
        this.roadCount++;
        
        var tile = 'tile_road_1';
        var isObstacle = false;
        
        if (this.roadCount > this.nextObstacleIndex && this.hasStarted) {
            this.calculateNextObstacleIndex();
            tile = 'obstacle_1';
            isObstacle = true;
        };

        // Create tiles call.
        this.addTileAtIndex(new TTTBuilding(this.game, 0, 0), 0);
        this.createTileAtIndex('tile_road_1', 1);
        this.createTileAtIndex('empty', 2);
        this.addTileAtIndex(new TTTBuilding(this.game, 0, 0), 3);
        this.createTileAtIndex('empty', 5);
        this.createTileAtIndex('empty', 7);
        this.createTileAtIndex('water', 8);
        
        // Create green near the road.
        this.createTileAtIndex(this.rightQueueOrEmpty(), 6);
        
        var sprite = this.createTileAtIndex(tile, 4);
        this.arrLayers.push(sprite);
        
        if (isObstacle) {
            this.arrObstacles.push(sprite);
        }
    };
    
    // Generate green queue.
    TTTGame.prototype.generateGreenQueue = function() {
        var retval = [];
        
        retval.push('green_start');
        
        // Random amount of middle tiles.
        var middle = Math.round(Math.random() * 2);
        var i = 0;
        while (i < middle) {
            retval.push('green_middle_empty');
            i++;
        }
        
        // Random amount of trees.
        var numberOfTrees = Math.round(Math.random() * 2);
        i = 0;
        while (i < numberOfTrees) {
            retval.push('green_middle_tree');
            i++;
        }
        
        retval.push('green_end');
        
        return retval;
    };
    
    // Generate right queue.
    TTTGame.prototype.generateRightQueue = function() {
        var minimumOffset = 5;
        var maximumOffset = 10;
        var num = Math.random() * (maximumOffset - minimumOffset);
        this.nextQueueIndex = this.roadCount + Math.round(num) + minimumOffset;
        this.rightQueue.push(this.generateGreenQueue());
    };
    
    // Right queue or empty.
    TTTGame.prototype.rightQueueOrEmpty = function() {
        var retval = 'empty';
        
        if (this.rightQueue.length !== 0) {
            
            // RightQueue is a multi-dimensional array.
            retval = this.rightQueue[0][0];
            
            this.rightQueue[0].splice(0, 1);
            
            if (this.rightQueue[0].length === 0) {
                this.rightQueue.splice(0, 1);
            }
        }
        
        return retval;
    };
    
    // Calculate obstacl position.
    TTTGame.prototype.calculateNextObstacleIndex = function() {
        var minimumOffset = 3;
        var maximumOffset = 10;
        var num = Math.random() * (maximumOffset - minimumOffset);
        this.nextObstacleIndex = this.roadCount + Math.round(num) + minimumOffset;
    };
    
    // Move tiles.
    TTTGame.prototype.moveTilesWithSpeed = function(speed) {
        var i = this.arrLayers.length - 1;
        
        // Reverse loop over all the tiles.
        while (i >= 0) {
            
            this.arrTiles = this.arrLayers[i].children;
            var j = this.arrTiles.length - 1;
            
            while (j >= 0) {
                var sprite = this.arrTiles[j];
                // Move the sprite.
                sprite.x -= speed * Math.cos(ANGLE * Math.PI / 180);
                sprite.y += speed * Math.sin(ANGLE * Math.PI / 180);
                
                if (sprite.x < -120) {
                    // We don't need to splice anymore
                    this.arrLayers[i].removeChild(sprite);
                    sprite.destroy();
                }
                j--;
            }
            i--;
        }
    };
    
    // Calculation X position.
    TTTGame.prototype.calculatePositionOnRoadWithXposition = function(xPos) {
        // Calculate our triangle
        var adjacent = this.roadStartPosition.x - xPos;
        var alpha = ANGLE * Math.PI / 180;
        var hypotenuse = adjacent / Math.cos(alpha);
        var opposite = Math.sin(alpha) * hypotenuse;
        
        return {
            x: xPos,
            // -57 to position the taxi on the road
            y: this.roadStartPosition.y + opposite - 57
        };
    };
    
    // Calculate Taxi POsition.
    TTTGame.prototype.calculateTaxiPosition = function() {
        var multiplier = 0.025;
        var num = TAXI_START_X + (this.scoreCount * GAME_WIDTH * multiplier);
        
        // Limit it to 60% of the game width.
        if (num > GAME_WIDTH * 0.60) {
            num = GAME_WIDTH * 0.60;
        }
        
        // Assign the target X value to taxiTarget.
        this.taxiTargetX = num;
        
        // Gradually increase taxiX to approach taxiTargetX.
        if (this.taxiX < this.taxiTargetX) {
            this.taxiX += (this.taxiTargetX - this.taxiX) / EASING;
        }
    };
    
    // Jump function.
    TTTGame.prototype.taxiJump = function() {
        // console.log('taxiJump: ', this.currentJumpHeight, this.jumpSpeed);
        this.currentJumpHeight -= this.jumpSpeed;
        this.jumpSpeed -= 0.5;
        
        if (this.jumpSpeed < - JUMP_HEIGHT) {
            this.jumpSpeed = JUMP_HEIGHT;
            this.isJumping = false;
        }
    };
    
    // Check obstacles hit.
    TTTGame.prototype.checkObstacles = function() {
        var i = this.arrObstacles.length - 1;
        
        while (i >= 0) {
            var sprite = this.arrObstacles[i];
            
            // We don't want to check on items, that are past the taxi
            if (sprite.x < this.taxi.x - 10) {
                this.arrObstacles.splice(i, 1);
                
                // Increase the score.
                this.scoreCount++;
                this.sfx.score.play();
                
                // Set the score & animate it!
                this.counter.setScore(this.scoreCount, true);
            }
            
            // Distance formula.
            var dx = sprite.x - this.taxi.x;
            dx = Math.pow(dx, 2);
            
            var dy = (sprite.y - sprite.height / 2) - this.taxi.y;
            dy = Math.pow(dy, 2);
            
            var distance = Math.sqrt(dx + dy);
            
            if (distance < 25) {
                if (!this.isDead) {
                    this.gameOver();
                }
            }
            
            i--;
        }
    };
    
    // Game over.
    TTTGame.prototype.gameOver = function() {
        this.sfx.hit.play();
        this.isDead = true;
        this.hasStarted = false;
        this.arrObstacles = [];
        var dieSpeed = SPEED / 10;
        
        var tween_1 = this.game.add.tween(this.taxi);
        tween_1.to({
            x: this.taxi.x + 20,
            y: this.taxi.y - 40
        }, 300 * dieSpeed, Phaser.Easing.Quadratic.Out);
        
        var tween_2 = this.game.add.tween(this.taxi);
        tween_2.to({
            y: GAME_HEIGHT + 40
        }, 1000 * dieSpeed, Phaser.Easing.Quadratic.In);
        
        tween_1.chain(tween_2);
        tween_1.start();
        
        var tween_rotate = this.game.add.tween(this.taxi);
        tween_rotate.to({
            angle: 200
        }, 1300 * dieSpeed, Phaser.Easing.Linear.None);
        
        tween_rotate.start();
        
        this.blackOverlay.alpha = 0.6;
        this.blackOverlay.visible = true;
        
        this.gameOverGraphic.visible = true;
        this.buttonRestart.visible = true;
    };
    
    // Touch DOWN.
    TTTGame.prototype.touchDown = function() {
        this.mouseTouchDown = true;
        
        if (!this.isJumping) {
            this.isJumping = true;
            this.sfx.jump.play();
        }
        
        if (!this.hasStarted) {
            this.startGame();
        }
    };
    
    // Touch UP.
    TTTGame.prototype.touchUp = function() {
        this.mouseTouchDown = false;  
    };
    
    // Restart function.
    TTTGame.prototype.restart = function() {
        this.reset();
    };
    
    return TTTGame;
})();